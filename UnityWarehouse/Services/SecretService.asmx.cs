﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace UnityWarehouse.Services
{
    /// <summary>
    /// Summary description for SecretService
    /// </summary>
    [WebService(Namespace = "http://somewhere.azurewebsites.net")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class SecretService : System.Web.Services.WebService
    {

        /// <summary>
        /// This is just a placeholder, I would have either a stored pregenerated list and hand out codes when they are needed and expire them shortly if not used,
        /// or I would regenerate a code each time. Another option would be proof of work, say sending them a hash that came from some number of a size that would require 
        /// a decent amount of work to compute, making sure that any use of the system costs the requester more than it costs us to provide.
        /// </summary>
        /// <returns>Returns a code if from a whitelisted IP or empty otherwise</returns>
        [WebMethod]
        public string GetOneTimeuseCode()
        {
            string code = "";

            if(Context.Request.UserHostAddress == "::1") 
            {
                code = GenerateNewCode();
            }

            return code;
        }

        [WebMethod]
        public static bool VerifyCode(string code)
        {
            return CurrentlyValidCodes().Contains(code);
        }

        public static List<string> CurrentlyValidCodes()
        {
            //A placeholder for something that would hold one time use codes, real implementation would store them somewhere or have a deterministic generation
            return new List<string> { "somecode" };
        }

        private string GenerateNewCode()
        {
            //A real implementation would insert this into the stored valid codes
            return "somecode";
        }
    }
}
