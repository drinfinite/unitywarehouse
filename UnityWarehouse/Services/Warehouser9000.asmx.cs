﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ArbitraryCustomerProject;
using Newtonsoft.Json;
using Data;
using ArbitraryCustomerProject;

namespace UnityWarehouse.Services
{
    /// <summary>
    /// Summary description for Warehouser9000
    /// </summary>
    [WebService(Namespace = "http://localhost:8080/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Warehouser9000 : WebService
    {
        [WebMethod(Description = "Returns a string of the cart or null") ]
        public string GetCart(string guid)
        {
            return JsonConvert.SerializeObject(CartHandler.GetCart(guid));
        }

        [WebMethod(Description = "Tasks that require large amounts of compute or incur other kinds of costs require an OTP key.")]
        public string CreateCart(string guid, string firstName, string lastName, string fullAddress, int itemID, string OTP)
        {
            Cart cart = null;

            if (SecretService.VerifyCode(OTP))
            {
                OrderHeader orderHeader = OrderHeaderHandler.CreateOrderHeader(firstName, lastName, fullAddress);
                Item item = ItemHandler.GetItem(itemID);
                cart = CartHandler.CreateCart(guid, orderHeader.id, itemID, item.aisle, item.shelfLevel);
            }

            return JsonConvert.SerializeObject(cart);
        }

        [WebMethod(Description = "Creates a delivery in the connected database.")]
        public string ReceiveDelivery(DateTime deliveredAt, string deliveredBy, string receivedAt, bool deliveryDamaged)
        {
            return JsonConvert.SerializeObject(DeliveryHandler.CreateDelivery(deliveredAt, deliveredBy, receivedAt, deliveryDamaged)); 
        }

        [WebMethod(Description = "Creates an item that can be later added to invoices.")]
        public string CreateItem(int sourceFromPalletID, DateTime createdAt, int aisle, int shelfLevel, int quantityInStock)
        {
            return JsonConvert.SerializeObject(ItemHandler.CreateItem(sourceFromPalletID, createdAt, aisle, shelfLevel, quantityInStock));
        }

        [WebMethod(Description = "Creates a pallet that is received in a delivery.")]
        public string CreatePallet(int deliveryID)
        {
            return JsonConvert.SerializeObject(PalletHandler.CreatePallet(deliveryID));
        }

        [WebMethod(Description = "Creates a customer that can then create carts.")]
        public string CreateCustomer(string email, string password, string TFAPhone)
        {
            return JsonConvert.SerializeObject(CustomerHandler.CreateCustomer(email, password, TFAPhone));
        }
    }
}
