﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ArbitraryCustomerProject;
using Newtonsoft.Json;

namespace UnityWarehouse.Services
{
    public class CustomerController : ApiController
    {
        [Route("customer/getall")]
        [HttpGet]
        public List<Customer> GetAll()
        {
            List<Customer> customers = CustomerHandler.GetCustomers();
            return customers;
        }

        [Route("customer/get")]
        [HttpGet]
        public Customer Get(int id)
        {
            return CustomerHandler.GetCustomer(id);
        }

        [Route("customer/create")]
        [HttpPost]
        public void PostBody([FromBody]string value)
        {
            try
            {
                Customer customer = JsonConvert.DeserializeObject<Customer>(value);

                if (customer.id > 0)
                {
                    Ok();
                }
            }
            catch(InvalidOperationException ex)
            {
                Helper.RecordError(ex);
            }
            catch(NullReferenceException nex)
            {
                Helper.RecordError(nex);
            }
        }

        [Route("customer/examples")]
        [HttpPost]
        public int PostURI([FromUri]string value)
        {
            int.TryParse(value, out int number);

            if (number > Singleton.Instance.minimumNumber)
            {
                unsafe
                {
                    int* pointy = &number; //just as an example, get the pointer for the number we just allocated
                    *pointy ^= *pointy; //an integral type XOR
                    uint anotherNumber = (uint)pointy; //typecasting to another type
                    anotherNumber = anotherNumber >> 2; //bitshifting for style points
                    number = (int)anotherNumber;
                }
            }

            return number;
        }
    }

    public sealed class Singleton
    {
        private static readonly Lazy<Singleton> lazy = new Lazy<Singleton> (() => new Singleton());
        public int minimumNumber;

        public static Singleton Instance {
            get { return lazy.Value; }
        }

        private Singleton()
        {
            minimumNumber = -1;
        }
    }
}