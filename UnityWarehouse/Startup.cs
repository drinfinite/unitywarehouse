﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UnityWarehouse.Startup))]
namespace UnityWarehouse
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
