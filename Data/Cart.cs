namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cart")]
    public partial class Cart
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cart()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string cartGUID { get; set; }

        public int orderID { get; set; }

        public int itemID { get; set; }

        public int pickFromAisle { get; set; }

        public int pickFromShelfLevel { get; set; }

        public virtual OrderHeader OrderHeader { get; set; }

        public virtual OrderHeader OrderHeader1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
