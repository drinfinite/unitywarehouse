﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class ItemHandler
    {
        public static Item GetItem(int id)
        {
            Item item = null;

            try
            {
                using (var db = new WarehouseModel())
                {
                    item = db.Items.Find(id);
                }
            }
            catch(Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                item = null;

            }

            return item;
        }

        public static Item CreateItem(int sourceFromPalletID, DateTime? createdAt, int aisle, int shelfLevel, int quantityInStock)
        {
            //CreatedAt time is a null coalescing example
            Item item = new Item() { sourcedFromPalletID = sourceFromPalletID, createdAt = createdAt ?? DateTime.Now, aisle = aisle, shelfLevel = shelfLevel, quantityInStock = quantityInStock < 0 ? quantityInStock : 0 };

            try
            {
                using (var db = new WarehouseModel())
                {
                    item = db.Items.Add(item);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                item = null;
            }

            return item;
        }
    }
}
