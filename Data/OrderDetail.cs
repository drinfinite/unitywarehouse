namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrderDetail
    {
        public int id { get; set; }

        public int cartID { get; set; }

        public int itemID { get; set; }

        public int quantityOfItems { get; set; }

        public virtual Cart Cart { get; set; }

        public virtual Item Item { get; set; }
    }
}
