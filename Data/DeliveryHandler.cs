﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DeliveryHandler
    {
        public static Delivery GetDelivery(int id)
        {
            Delivery delivery = null;

            try
            {
                using (var db = new WarehouseModel())
                {
                    delivery = db.Deliveries.Find(id);
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                delivery = null;

            }

            return delivery;
        }

        public static Delivery CreateDelivery(DateTime deliveredAt, string deliveredBy, string receivedBy, bool deliveryDamaged)
        {
            Delivery delivery = new Delivery();
            delivery.deliveredAt = deliveredAt;
            delivery.deliveredBy = deliveredBy.Length <= 800 ? deliveredBy : deliveredBy.Substring(0, 799);
            delivery.receivedBy = receivedBy.Length <= 800 ? receivedBy : receivedBy.Substring(0, 799);
            delivery.deliveryDamaged = deliveryDamaged;

            try
            {
                using (var db = new WarehouseModel())
                {
                    delivery = db.Deliveries.Add(delivery);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                delivery = null;
            }

            return delivery;
        }
    }
}