﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public static class CartHandler
    {
        /// <summary>
        /// Finds the cart in the Warehouse database
        /// </summary>
        /// <param name="guid">The primary key 'id'</param>
        /// <returns>Cart object, filled or null</returns>
        public static List<Cart> GetCart(string guid)
        {
            List<Cart> cart = new List<Cart>();

            try
            {
                using (var db = new WarehouseModel())
                {
                    cart = db.Carts.Where(cartFuncExpressionProxy => cartFuncExpressionProxy.cartGUID == guid).ToList();
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
            }

            return cart;
        }

        /// <summary>
        /// Creates a cart part in the connected database
        /// </summary>
        /// <param name="cartGUID">What this cart part is associated with, many carts with the same GUID are acted upon at the same time.</param>
        /// <param name="orderID">The foreign key ID of an order relating to picking that this is tied to</param>
        /// <param name="itemID">An ID related to 0 or 1 kinds of items</param>
        /// <param name="pickFromAisle">A physical location to pick from</param>
        /// <param name="pickFromShelfLevel">A more specific location in the aisle to pick from</param>
        /// <returns>The Cart object, or null on failure</returns>
        public static Cart CreateCart(string cartGUID, int orderID, int itemID, int pickFromAisle, int pickFromShelfLevel)
        {
            Cart cart = new Cart();
            cart.cartGUID = cartGUID;
            cart.orderID = orderID;
            cart.pickFromAisle = pickFromAisle;
            cart.pickFromShelfLevel = pickFromShelfLevel;

            return CreateCart(cart);
        }

        public static Cart CreateCart(Cart cart)
        {
            cart.cartGUID = cart.cartGUID.Length <= 20 ? cart.cartGUID : cart.cartGUID.Substring(0, 19);

            try
            {
                using (var db = new WarehouseModel())
                {
                    cart = db.Carts.Add(cart);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                cart = null;
            }

            return cart;
        }
    }
}
