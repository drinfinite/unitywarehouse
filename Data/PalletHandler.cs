﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class PalletHandler
    {
        public static Pallet GetPallet(int id)
        {
            Pallet pallet = null;

            try
            {
                using (var db = new WarehouseModel())
                {
                    pallet = db.Pallets.Find(id);
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                pallet = null;

            }

            return pallet;
        }

        public static Pallet CreatePallet(int deliveryID)
        {
            Pallet pallet = new Pallet() { deliveryID = deliveryID };
            try
            {
                using (var db = new WarehouseModel())
                {
                    pallet = db.Pallets.Add(pallet);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                pallet = null;
            }

            return pallet;
        }
    }
}
