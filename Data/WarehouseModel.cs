namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WarehouseModel : DbContext
    {
        public WarehouseModel()
            : base("name=WarehouseModel")
        {
            Database.Connection.ConnectionString = Database.Connection.ConnectionString.Replace("user id=exampleuser;password=exampledummytextthatisnottherealpassword", "user id=" + System.Environment.GetEnvironmentVariable("Unity3dWarehouseUsername") + ";password=" + Environment.GetEnvironmentVariable("Unity3dWarehousePassword"));
        }

        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Delivery> Deliveries { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderHeader> OrderHeaders { get; set; }
        public virtual DbSet<Pallet> Pallets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cart>()
                .Property(e => e.cartGUID)
                .IsUnicode(false);

            modelBuilder.Entity<Cart>()
                .HasMany(e => e.OrderDetails)
                .WithRequired(e => e.Cart)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.deliveredBy)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.receivedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .HasMany(e => e.Pallets)
                .WithRequired(e => e.Delivery)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Item>()
                .HasMany(e => e.OrderDetails)
                .WithRequired(e => e.Item)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderHeader>()
                .Property(e => e.firstName)
                .IsUnicode(false);

            modelBuilder.Entity<OrderHeader>()
                .Property(e => e.lastName)
                .IsUnicode(false);

            modelBuilder.Entity<OrderHeader>()
                .Property(e => e.fullAddress)
                .IsUnicode(false);

            modelBuilder.Entity<OrderHeader>()
                .HasMany(e => e.Carts)
                .WithRequired(e => e.OrderHeader)
                .HasForeignKey(e => e.itemID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderHeader>()
                .HasMany(e => e.Carts1)
                .WithRequired(e => e.OrderHeader1)
                .HasForeignKey(e => e.orderID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pallet>()
                .HasMany(e => e.Items)
                .WithRequired(e => e.Pallet)
                .HasForeignKey(e => e.sourcedFromPalletID)
                .WillCascadeOnDelete(false);
        }
    }
}
