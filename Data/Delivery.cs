namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Delivery")]
    public partial class Delivery
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Delivery()
        {
            Pallets = new HashSet<Pallet>();
        }

        public int id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime deliveredAt { get; set; }

        [Required]
        [StringLength(800)]
        public string deliveredBy { get; set; }

        [Required]
        [StringLength(800)]
        public string receivedBy { get; set; }

        public bool deliveryDamaged { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pallet> Pallets { get; set; }
    }
}
