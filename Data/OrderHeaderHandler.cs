﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class OrderHeaderHandler
    {
        public static OrderHeader GetOrderHeader(int id)
        {
            OrderHeader orderHeader = null;

            try
            {
                using (var db = new WarehouseModel())
                {
                    orderHeader = db.OrderHeaders.Find(id);
                }
            }
            catch (Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
                orderHeader = null;
            }

            return orderHeader;
        }

        public static OrderHeader CreateOrderHeader(string firstName, string lastName, string fullAddress)
        {
            OrderHeader orderHeader = new OrderHeader() { firstName = firstName, lastName = lastName, fullAddress = fullAddress };

            return orderHeader;
        }

        public static OrderHeader CreateOrderHeader(OrderHeader orderHeader)
        {
            orderHeader.firstName = orderHeader.firstName.Length <= 100 ? orderHeader.firstName : orderHeader.firstName.Substring(0, 99);
            orderHeader.lastName = orderHeader.lastName.Length <= 100 ? orderHeader.lastName : orderHeader.lastName.Substring(0, 99);
            orderHeader.fullAddress = orderHeader.fullAddress.Length <= 400 ? orderHeader.fullAddress : orderHeader.fullAddress.Substring(0, 399);

            try
            {
                using (var db = new WarehouseModel())
                {
                    orderHeader = db.OrderHeaders.Add(orderHeader);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Helper.MakeWarehouseErrorReport(ex);
            }

            return orderHeader;
        }
    }
}
