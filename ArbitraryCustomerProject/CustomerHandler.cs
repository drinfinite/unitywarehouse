﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbitraryCustomerProject
{
    public class CustomerHandler
    {
        /// <summary>
        /// Finds the customer in the Customer database
        /// </summary>
        /// <param name="id">The primary key 'id'</param>
        /// <returns>Customer object, filled or null</returns>
        public static Customer GetCustomer(int id)
        {
            Customer customer = null;

            try
            {
                using (var db = new CustomerModel())
                {
                    customer = db.Customers.Find(id);
                }
            }
            catch(Exception ex)
            {
                Helper.RecordError(ex);
            }

            return customer;
        }

        public static List<Customer> GetCustomers()
        {
            List<Customer> customers = null;

            try
            {
                using (var db = new CustomerModel())
                {
                    customers = db.Customers.ToList();
                }
            }
            catch (Exception ex)
            {
                Helper.RecordError(ex);
            }

            return customers;
        }

        /// <summary>
        /// Takes in the parts that make up a customer and tries to create one in the connected database
        /// </summary>
        /// <param name="email">The customers email which has to be unique</param>
        /// <param name="sessionKey">The session key which is granted to maintain login status</param>
        /// <param name="passwordSHA">The challenge to complete to log in</param>
        /// <param name="TFAPhone">A number to later connect to a TFA service like Twilio</param>
        /// <returns>The created customer object, or null on failure</returns>
        private static Customer CreateCustomer(string email, string sessionKey, string passwordSHA, string TFAPhone)
        {
            //If the item might not be valid coming in (say through length) this entry point wouldn't be my preferred place of modifying the fields, I would go to the lowest point and do the checks and balances there
            return CreateCustomer(new Customer { email = email, sessionKey = sessionKey, passwordSHA = passwordSHA, TFAPhone = TFAPhone });
        }

        /// <summary>
        /// Creates a customer with this overloaded function
        /// </summary>
        /// <param name="customer">The filled, valid customer object</param>
        /// <returns>The created customer object, or null on failure</returns>
        /// <exception cref="">Accesses string fields in ways that require all fields to not be null</exception>
        private static Customer CreateCustomer(Customer customer)
        {
            try
            {
                //These lines are here because a customer email exceeding the varchar length could cause the entry to not successfully insert
                customer.email = customer.email.Length <= 100 ? customer.email : customer.email.Substring(0, 99);
                customer.passwordSHA = customer.passwordSHA.Length <= 100 ? customer.passwordSHA : customer.passwordSHA.Substring(0, 99);
                customer.sessionKey = customer.sessionKey.Length <= 100 ? customer.sessionKey : customer.sessionKey.Substring(0, 99);
                customer.TFAPhone = customer.TFAPhone.Length <= 100 ? customer.TFAPhone : customer.TFAPhone.Substring(0, 99);
                using (var db = new CustomerModel())
                {
                    Customer naiveCustomerCheck = db.Customers.FirstOrDefault(customerProxyForLambda => customerProxyForLambda.email.Equals(customer.email, StringComparison.OrdinalIgnoreCase));
                    if (naiveCustomerCheck == new Customer())
                    {
                        customer = db.Customers.Add(customer);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Helper.RecordError(ex);
                customer = null;
            }

            return customer;
        }

        public static Customer CreateCustomer(string email, string password, string TFAPhone)
        {
            Customer customer = new Customer();

            try
            {
                customer.email = email;
                customer.TFAPhone = TFAPhone;

                string passwordSHA = "";



            }
            catch(Exception ex)
            {
                Helper.RecordError(ex);
            }

            return customer;
        }
    }
}
