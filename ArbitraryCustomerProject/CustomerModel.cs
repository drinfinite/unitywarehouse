namespace ArbitraryCustomerProject
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CustomerModel : DbContext
    {
        public CustomerModel()
            : base("name=CustomerModel")
        {
            Database.Connection.ConnectionString = Database.Connection.ConnectionString.Replace("user id=exampleuser;password=exampledummytextthatisnottherealpassword", "user id=" + System.Environment.GetEnvironmentVariable("Unity3dWarehouseUsername") + ";password=" + Environment.GetEnvironmentVariable("Unity3dWarehousePassword"));
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<PurchaseEvent> PurchaseEvents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.sessionKey)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.passwordSHA)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.TFAPhone)
                .IsUnicode(false);
        }
    }
}
