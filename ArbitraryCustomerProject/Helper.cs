﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbitraryCustomerProject
{
    public class Helper
    {
        public static void RecordError(Exception ex)
        {
            //No error reporting due to time constraints
            //The idea is to send off details about the exception to some place
            //For some exceptions send to a database, for some (like database timeout) send to some other service, for high severity send to a dev's email, etc
        }
    }
}
