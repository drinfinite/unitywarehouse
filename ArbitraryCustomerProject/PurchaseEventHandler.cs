﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbitraryCustomerProject
{
    public class PurchaseEventHandler
    {
        /// <summary>
        /// Finds the purchase event in the Customer database
        /// </summary>
        /// <param name="id">The primary key 'id'</param>
        /// <returns>PurchaseEvent object, filled or null</returns>
        public static PurchaseEvent GetPurchaseEvent(int id)
        {
            PurchaseEvent purchaseEvent = null;

            try
            {
                using (var db = new CustomerModel())
                {
                    purchaseEvent = db.PurchaseEvents.Find(id);
                }
            }
            catch (Exception ex)
            {
                Helper.RecordError(ex);
            }

            return purchaseEvent;
        }

        /// <summary>
        /// Creates a purchase event with an int that is not necessarily unique due to it's arbitrarily disjointed status
        /// </summary>
        /// <param name="arbitraryDisjointedPurchaseID">The disjoint, it's so arbitrary! This int can point to one or zero purchase orders in the other database</param>
        /// <param name="startedAt">Can be started at a time you set, or defaults to now if a null is passed through</param>
        /// <returns>PurchaseEvent object, filled or null on failure</returns>
        public static PurchaseEvent CreatePurchaseEvent(int arbitraryDisjointedPurchaseID, DateTime? startedAt)
        {
            PurchaseEvent purchaseEvent = new PurchaseEvent();
            purchaseEvent.arbiraryDisjointedPurchaseID = arbitraryDisjointedPurchaseID;
            purchaseEvent.startedAt = startedAt.HasValue ? startedAt.Value : DateTime.Now;

            return CreatePurchaseEvent(purchaseEvent);
        }

        /// <summary>
        /// This method creates a purchase order in the connected database in an overloaded function.
        /// </summary>
        /// <param name="purchaseEvent">An assumedly valid PurchaseEvent object</param>
        /// <returns>PurchaseEvent object, filled or null</returns>
        public static PurchaseEvent CreatePurchaseEvent(PurchaseEvent purchaseEvent)
        {
            try
            {
                using (var db = new CustomerModel())
                {
                    purchaseEvent = db.PurchaseEvents.Add(purchaseEvent);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Helper.RecordError(ex);
                purchaseEvent = null;
            }

            return purchaseEvent;
        }
    }
}
