namespace ArbitraryCustomerProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PurchaseEvent")]
    public partial class PurchaseEvent
    {
        public int id { get; set; }

        public int arbiraryDisjointedPurchaseID { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime startedAt { get; set; }
    }
}
