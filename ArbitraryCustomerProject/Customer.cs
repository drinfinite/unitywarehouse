namespace ArbitraryCustomerProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        public int id { get; set; }

        [Required]
        [StringLength(100)]
        public string email { get; set; }

        [Required]
        [StringLength(100)]
        public string sessionKey { get; set; }

        [Required]
        [StringLength(100)]
        public string passwordSHA { get; set; }

        [StringLength(100)]
        public string TFAPhone { get; set; }
    }
}
