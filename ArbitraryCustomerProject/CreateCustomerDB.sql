use unity3dcustomerdatabase;

CREATE TABLE Customer(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Customer PRIMARY KEY CLUSTERED,
	email VARCHAR(100) NOT NULL,
	sessionKey VARCHAR(100) NOT NULL,
	passwordSHA VARCHAR(100) NOT NULL,
	TFAPhone VARCHAR(100) NULL
)

CREATE TABLE PurchaseEvent(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_PurchaseEvent PRIMARY KEY CLUSTERED,
	arbiraryDisjointedPurchaseID INT NOT NULL, /*Intercommunication-required field*/
	startedAt DATETIME2 NOT NULL
)