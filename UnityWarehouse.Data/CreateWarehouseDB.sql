use unity3dwarehousedatabase;

/*All pallets are delivered*/
CREATE TABLE Delivery(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Delivery PRIMARY KEY CLUSTERED,
	deliveredAt DATETIME2 NOT NULL,
	deliveredBy VARCHAR(800) NOT NULL, /*Some hypothetical company or person*/
	receivedBy VARCHAR(800) NOT NULL, /*Some hypothetical company or person*/
	deliveryDamaged BIT NOT NULL
)

/*Pallets have different items, but an implicit location inside the warehouse*/
CREATE TABLE Pallet(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Pallet PRIMARY KEY CLUSTERED,
	deliveryID INT NOT NULL CONSTRAINT FK_PalletToDelivery FOREIGN KEY REFERENCES Delivery(id)
)

/*Once a pallet is broken down, it becomes inventory, and the location is no longer implicit*/
CREATE TABLE Item(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Item PRIMARY KEY CLUSTERED,
	sourcedFromPalletID INT NOT NULL CONSTRAINT FK_InventoryToPallet FOREIGN KEY REFERENCES Pallet(id),
	createdAt DATETIME2 NOT NULL,
	aisle INT NOT NULL,
	shelfLevel INT NOT NULL,
	quantityInStock INT NOT NULL
)

CREATE TABLE OrderHeader(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_OrderHeader PRIMARY KEY CLUSTERED,
	firstName VARCHAR(100) NOT NULL,
	lastName VARCHAR(100) NOT NULL,
	fullAddress VARCHAR(400) NOT NULL
)

CREATE TABLE Cart(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Cart PRIMARY KEY CLUSTERED,
	cartGUID VARCHAR(20) NOT NULL,
	orderID INT NOT NULL CONSTRAINT FK_CartToOrderHeader FOREIGN KEY REFERENCES OrderHeader(id),
	itemID INT NOT NULL CONSTRAINT FK_CartToItem FOREIGN KEY REFERENCES OrderHeader(id),
	pickFromAisle INT NOT NULL,
	pickFromShelfLevel INT NOT NULL
)

CREATE TABLE OrderDetails(
	id INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_OrderDetails PRIMARY KEY CLUSTERED,
	cartID INT NOT NULL CONSTRAINT FK_OrderDetailsToCart FOREIGN KEY REFERENCES Cart(id),
	itemID INT NOT NULL CONSTRAINT FK_OrderDetailsToItem FOREIGN KEY REFERENCES Item(id),
	quantityOfItems INT NOT NULL
)